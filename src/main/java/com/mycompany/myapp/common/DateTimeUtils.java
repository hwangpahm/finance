package com.mycompany.myapp.common;

import com.mycompany.myapp.common.constants.DateFormatConstant;
import com.mycompany.myapp.component.dto.DateDurationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class DateTimeUtils {

    public static String formatTime(Date date, String format) {
        if (date == null) return null;
        DateFormat simpleFormatter = new SimpleDateFormat(format);
        return simpleFormatter.format(date);
    }

    public static Date parseDate(String datetime, String format) {
        try {
            if (!StringUtils.hasLength(datetime) || !StringUtils.hasLength(format)) {
                return null;
            }
            DateFormat dateFormatter = new SimpleDateFormat(format);
            return dateFormatter.parse(datetime);
        } catch (ParseException e) {
            log.error("ParseException: {}", e.getMessage());
            return null;
        }
    }

    public static String convertDateFormat(String dateStr, String sourceFormat, String destinationFormat) {
        if (!StringUtils.hasText(dateStr)) {
            return dateStr;
        }
        Date date = DateTimeUtils.parseDate(dateStr, sourceFormat);
        if (date == null) {
            return dateStr;
        }
        return DateTimeUtils.formatTime(date, destinationFormat);
    }

    public static DateDurationDTO getStartDateAndEndDate(String yearMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateTimeUtils.parseDate(yearMonth, DateFormatConstant.MONTH_FORMAT_YYYYMM));

        Date monthFirstDay = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date monthLastDay = calendar.getTime();
        return DateDurationDTO.builder()
            .startDate(monthFirstDay)
            .endDate(monthLastDay)
            .build();

    }


}
