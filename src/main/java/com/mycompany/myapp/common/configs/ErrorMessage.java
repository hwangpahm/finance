package com.mycompany.myapp.common.configs;

import com.mycompany.myapp.common.constants.ErrorCategory;
import com.mycompany.myapp.error.exceptions.ErrorBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.Locale;

@Slf4j
@Component
public class ErrorMessage {
    private static final String ERROR_CODE_NOT_FOUND = "ERR???";
    private static final String MESSAGE_NOT_FOUND = "Message not found key: {0}";
    private static final String DELIMITER = "~";

    private final MessageSource messageSource;
    private final HttpServletRequest request;
    private AcceptHeaderLocaleResolver acceptHeaderLocaleResolver = new AcceptHeaderLocaleResolver();

    public ErrorMessage(MessageSource messageSource, HttpServletRequest request) {
        this.messageSource = messageSource;
        this.request = request;
    }

    private Locale getLocale() {
        acceptHeaderLocaleResolver.setDefaultLocale(Locale.JAPAN);
        return acceptHeaderLocaleResolver.resolveLocale(request);
    }

    public ErrorBase getError(String key) {
        return getError(key, null, null, getLocale());
    }

    public ErrorBase getError(String key, String fieldName) {
        return getError(key, fieldName, null, getLocale());
    }

    public ErrorBase getError(String key, String fieldName, Object[] args) {
        return getError(key, fieldName, args, getLocale());
    }

    public ErrorBase getError(String key, String fieldName, Object[] args, Locale locale) {
        try {
            String errorStr = messageSource.getMessage(key, args, locale);
            return createError(errorStr, key, fieldName);
        } catch (NoSuchMessageException e) {
            log.error("NoSuchMessageException: Message property key {}, field name: {}, details: {}, ", key, fieldName, e.getMessage());
            return new ErrorBase(ERROR_CODE_NOT_FOUND, MessageFormat.format(MESSAGE_NOT_FOUND, key), ErrorCategory.SYSTEM_CATEGORY);
        }
    }


    private ErrorBase createError(String message, String key, String fieldName) {
        if (StringUtils.hasText(message)) {
            String[] msg = message.split(DELIMITER);
            if (msg.length == 2) {
                return new ErrorBase(msg[0], msg[1], ErrorCategory.VALIDATE_CATEGORY, fieldName);
            } else {
                return new ErrorBase(ERROR_CODE_NOT_FOUND, message, ErrorCategory.VALIDATE_CATEGORY, fieldName);
            }
        }
        return new ErrorBase(ERROR_CODE_NOT_FOUND, MessageFormat.format(MESSAGE_NOT_FOUND, key), ErrorCategory.SYSTEM_CATEGORY, fieldName);
    }
}
