package com.mycompany.myapp.common.constants;

public final class ErrorCode {
    private ErrorCode() {
    }

    public static final String EMAIL_EXISTS = "validate.core.common.email.exists";
    public static final String LOGIN_EXISTS = "validate.core.common.login.exists";
    public static final String PASSWORD_INVALID = "validate.core.common.password.does.not.match";
    public static final String RECORD_NOT_FOUND = "validate.core.common.record.not.found";
    public static final class CategoryErrorCode {
        private CategoryErrorCode() {}

        public static final String CATEGORY_EXISTS = "validate.core.common.category.exists";
    }
}
