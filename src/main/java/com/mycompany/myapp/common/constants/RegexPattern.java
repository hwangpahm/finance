package com.mycompany.myapp.common.constants;

public final class RegexPattern {
    private RegexPattern() {}
    public static final String DATE_FORMAT_YYYMMDD = "^\\d{4}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$";

    public static final String DATE_FORMAT_YYYMM = "^\\d{4}(0[1-9]|1[0-2])$";

    public static final String DATE_FORMAT = "^\\d{4}/(0[1-9]|1[0-2])/(0[1-9]|[12][0-9]|3[01])$";
}
