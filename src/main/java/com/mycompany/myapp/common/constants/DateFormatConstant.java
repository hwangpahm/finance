package com.mycompany.myapp.common.constants;

public final class DateFormatConstant {
    private DateFormatConstant() {}

    public static final String TIME_FORMAT = "HH:mm:ss";

    public static final String DATE_FORMAT = "yyyy/MM/dd";

    public static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    public static final String DATETIME_FORMAT_WITHOUT_SECOND = "yyyy/MM/dd HH:mm";

    public static final String DATE_FORMAT_NOT_SPACE = "yyyyMMdd";

    public static final String TIME_HH_MM_FORMAT = "HH:mm";

    public static final String DATE_FORMAT_JP = "yyyy年MM月dd日";

    public static final String MONTH_FORMAT_YYYYMM = "yyyyMM";

}
