package com.mycompany.myapp.common.constants;

public class ErrorCategory {
    private ErrorCategory() {}

    public static final String VALIDATE_CATEGORY = "validate";
    public static final String BUSINESS_CATEGORY = "business";
    public static final String SYSTEM_CATEGORY = "system";

}
