package com.mycompany.myapp.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CategoryExpense implements Code <Integer> {

    FOOD(1),
    HOUSE_WARE(2),
    CLOTHES(3),
    COSMETIC(4),
    EXCHANGE(5),
    MEDICAL(6),
    EDUCATION(7),
    ELECTRIC_BILL(8),
    TRANSPORTATION(9),
    CONTACT_FEE(10),
    HOUSING_EXPENSE(11),
    OTHER(12);

    @Getter
    private final Integer code;

    public static CategoryExpense fromCode(Integer code) {
        return Code.fromCode(code, CategoryExpense.values());
    }

    public static CategoryExpense fromValue(String v) {
        if (v == null) return OTHER;
        return CategoryExpense.valueOf(v);

    }
}
