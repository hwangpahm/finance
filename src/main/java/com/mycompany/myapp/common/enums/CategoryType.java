package com.mycompany.myapp.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CategoryType implements Code<Integer> {

    INCOME(1),
    EXPENSE(2);

    @Getter
    private final Integer code;

    public static CategoryType fromCode(Integer code) {
        return Code.fromCode(code, CategoryType.values());
    }
}
