package com.mycompany.myapp.common.enums;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface Code<C> {
    static <C, T extends Code<C>> List<C> asList(T[] codes) {
        return Arrays.stream(codes).map(Code::getCode).collect(Collectors.toList());
    }

    static <T extends Code<?>> T fromCode(Object code, T[] codes) {
        return fromCode(code, codes, null);
    }

    static <T extends Code<?>> T fromCode(Object code, T[] codes, T defaultValue) {
        Predicate<T> predicate = code == null ? c -> c.getCode() == null
            : c -> c.getCode() != null && c.getCode().equals(code);
        return find(predicate, codes, defaultValue);
    }

    static <T extends Code<?>> T find(Predicate<T> predicate, T[] codes, T defaultValue) {
        return Arrays.stream(codes).filter(predicate).findFirst().orElse(defaultValue);
    }

    C getCode();
}
