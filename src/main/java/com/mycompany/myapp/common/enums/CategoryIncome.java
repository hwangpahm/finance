package com.mycompany.myapp.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CategoryIncome implements Code<Integer> {

    SALARY(1),
    POCKET_MONEY(2),
    BONUS(3),
    SIDE_JOB(4),
    INVESTMENT(5),
    EXTRA(7),
    OTHER(8);

    @Getter
    private final Integer code;

    public static CategoryIncome fromCode(Integer code) {
        return Code.fromCode(code, CategoryIncome.values());
    }

    public static CategoryIncome fromValue(String v) {
        if (v == null) return OTHER;
        return CategoryIncome.valueOf(v);

    }
}
