package com.mycompany.myapp.common.convert;

import com.mycompany.myapp.common.enums.CategoryExpense;
import com.mycompany.myapp.common.enums.CategoryIncome;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CategoryIncomeConverter implements AttributeConverter<CategoryIncome, Integer> {
    @Override
    public Integer convertToDatabaseColumn(CategoryIncome categoryIncome) {
        if (categoryIncome == null)
            return null;
        return categoryIncome.getCode();
    }

    @Override
    public CategoryIncome convertToEntityAttribute(Integer code) {
        if (code == null)
            return null;
        return CategoryIncome.fromCode(code);
    }
}
