package com.mycompany.myapp.common.convert;

import com.mycompany.myapp.common.enums.CategoryType;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CategoryTypeConverter implements AttributeConverter<CategoryType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CategoryType type) {
        if (type == null)
            return null;
        return type.getCode();
    }

    @Override
    public CategoryType convertToEntityAttribute(Integer code) {
        if (code == null)
            return null;
        return CategoryType.fromCode(code);
    }
}
