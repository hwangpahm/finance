package com.mycompany.myapp.common.convert;

import com.mycompany.myapp.common.enums.CategoryExpense;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CategoryExpenseConverter implements AttributeConverter<CategoryExpense, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CategoryExpense categoryExpense) {
        if (categoryExpense == null)
            return null;
        return categoryExpense.getCode();
    }

    @Override
    public CategoryExpense convertToEntityAttribute(Integer code) {
        if (code == null)
            return null;
        return CategoryExpense.fromCode(code);
    }
}
