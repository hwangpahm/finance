package com.mycompany.myapp.error;

import com.mycompany.myapp.common.constants.ErrorCategory;
import com.mycompany.myapp.component.dto.ErrorDTO;
import com.mycompany.myapp.error.exceptions.*;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final String ERROR_LOG_PREFIX = "ERROR:";

    /**
     * @return Method to handle UnsupportedMethodException
     */
    @ExceptionHandler(UnsupportedMethodException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleUnsupportedMethodException(UnsupportedMethodException e) {
        ErrorBase error = e.getError();
        error.setCategory(ErrorCategory.VALIDATE_CATEGORY);
        LOGGER.warn(error.getMessage());
        return new ErrorDTO(error);
    }

    /**
     * @return Method to handle ServiceFailedException
     */
    @ExceptionHandler(ServiceFailedException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ErrorDTO handleServiceFailedException(ServiceFailedException e) {
        ErrorBase error = e.getError();
        error.setCategory(ErrorCategory.SYSTEM_CATEGORY);
        if (error.getArguments() instanceof String[]) {
            error.setMessage(((String[]) error.getArguments())[0]);
        }
        LOGGER.error("ServiceFailedException -> ", e);
        return new ErrorDTO(error);
    }

    @ExceptionHandler(ConnectException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ErrorDTO handleServiceFailedException(
        ConnectException e) {
        ErrorBase error = e.getError();
        error.setCategory(ErrorCategory.SYSTEM_CATEGORY);
        if (error.getArguments() instanceof String[]) {
            error.setMessage(((String[]) error.getArguments())[0]);
        }
        LOGGER.error("ConnectException -> ", e);
        return new ErrorDTO(error);
    }

    /**
     * @return Method to handle ValidationException
     */
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleValidationException(ValidationException e) {
        List<ErrorBase> errors = new ArrayList<>();
        Set<String> errorSet = new HashSet<>();
        for (ErrorBase error : e.getAllErrors()) {
            if (errorSet.add(error.getMessage())) {
                errors.add(error);
                error.setCategory(ErrorCategory.BUSINESS_CATEGORY);
                LOGGER.warn(error.getCode(), error.getMessage());
            }
        }

        return new ErrorDTO(errors);
    }

    /**
     * @return Method to handle MethodArgumentNotValidException
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<ObjectError> objectErrors = e.getBindingResult().getAllErrors();
        List<ErrorBase> errors = new ArrayList<>(objectErrors.size());
        for (ObjectError objectError : objectErrors) {
            FieldError fieldError = (FieldError) objectError;
            String defaultMessage = fieldError.getDefaultMessage();
            String[] splitMsg = defaultMessage == null ? null : defaultMessage.split("~");
            if (splitMsg != null && splitMsg.length == 2) {
                String field = ((FieldError) objectError).getField();
                errors.add(new ErrorBase(splitMsg[0], splitMsg[1], ErrorCategory.VALIDATE_CATEGORY, field));
            } else {
                errors.add(new ErrorBase("ERR???", fieldError.getDefaultMessage(), ErrorCategory.SYSTEM_CATEGORY));
            }
        }
        return new ErrorDTO(errors);
    }

    /**
     * @return Method to handle HttpMessageNotReadableException
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        ErrorBase error = new ErrorBase("ERR251", "Request JSON Format is Invalid", ErrorCategory.VALIDATE_CATEGORY);
        LOGGER.warn("{} {} {}", error.getCode(), "Validation Failed: Request JSON Format is Invalid ",
            e.getMessage());
        return new ErrorDTO(error);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleMethodArgumentTypeMismatchException(MissingServletRequestParameterException e) {
        StringBuilder msg = new StringBuilder("Parameter [")
            .append(e.getParameterName())
            .append("] is required");
        ErrorBase error = new ErrorBase("ERR498", msg.toString(), ErrorCategory.VALIDATE_CATEGORY, e.getParameterName());
        return new ErrorDTO(error);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleMissingServletRequestParameterException(MethodArgumentTypeMismatchException e) {
        StringBuilder msg = new StringBuilder(e.getName()).append("が無効です。");
        ErrorBase error = new ErrorBase("ERR1000", msg.toString(), ErrorCategory.VALIDATE_CATEGORY, e.getName());
        return new ErrorDTO(error);
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ErrorDTO handleUnauthorizedException(UnauthorizedException e) {
        ErrorBase error = e.getError();
        if (error.getArguments() instanceof String[]) {
            error.setMessage(((String[]) error.getArguments())[0]);
        }
        LOGGER.error("UnauthorizedException -> ", e);
        return new ErrorDTO(error);
    }

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleConstraintViolationException(ConstraintViolationException e) {
        List<Object> objectErrors = new ArrayList<>(e.getConstraintViolations());
        List<ErrorBase> errors = new ArrayList<>(objectErrors.size());
        for (Object object : objectErrors) {
            ConstraintViolationImpl ex = (ConstraintViolationImpl)object;
            var pathImpl = ((PathImpl) ex.getPropertyPath());
            String[] splitMsg = ex.getMessage().split("~");
            if (splitMsg != null && splitMsg.length == 2) {
                StringBuilder msg = new StringBuilder(splitMsg[1]);
                errors.add(new ErrorBase(splitMsg[0], msg.toString(), ErrorCategory.VALIDATE_CATEGORY, pathImpl.getLeafNode().getName()));
            } else {
                errors.add(new ErrorBase("ERR???", ex.getMessage(), ErrorCategory.SYSTEM_CATEGORY, pathImpl.getLeafNode().getName()));
            }
        }
        return new ErrorDTO(errors);
    }

    @ExceptionHandler(UndeclaredThrowableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleValidationThrowable(UndeclaredThrowableException e) {
        if (e.getUndeclaredThrowable() instanceof ValidationException) {
            return handleValidationException((ValidationException) e.getUndeclaredThrowable());
        }
        return handleAnotherException(e);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorDTO handleRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        LOGGER.error(ERROR_LOG_PREFIX, e);
        return new ErrorDTO(new ErrorBase("ERR400", "Method not supported.", ErrorCategory.VALIDATE_CATEGORY));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ErrorDTO handleAnotherException(Exception e) {
        LOGGER.error(ERROR_LOG_PREFIX, e);
        return new ErrorDTO(new ErrorBase("ERR500", "Internal server error.", ErrorCategory.SYSTEM_CATEGORY));
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody
    ErrorDTO handleAccessDeniedException(AccessDeniedException e) {
        LOGGER.error("AccessDeniedException -> ", e);
        return new ErrorDTO(new ErrorBase("ERR403",e.getMessage(), ErrorCategory.SYSTEM_CATEGORY));
    }
}
