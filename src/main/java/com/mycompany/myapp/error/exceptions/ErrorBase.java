package com.mycompany.myapp.error.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorBase implements Serializable {
    private static final long serialVersionUID = -7370344933585245593L;

    private String code;

    private String message;

    private String category;

    private String fieldName;

    private transient Object[] params;

    @JsonIgnore
    private transient Object[] arguments;

    public ErrorBase(String code, String errorMessage) {
        this.code = code;
        this.message = errorMessage;
    }

    public ErrorBase(String code, String message, String category) {
        this.code = code;
        this.message = message;
        this.category = category;
    }

    public ErrorBase(String code, String message, String category, String fieldName) {
        this.code = code;
        this.message = message;
        this.category = category;
        this.fieldName = fieldName;
    }

    public ErrorBase(String code, String message, String category, String fieldName, Object... params) {
        this.code = code;
        this.message = message;
        this.category = category;
        this.fieldName = fieldName;
        this.params = params;
    }

    public ErrorBase(String code, String message, Object[] arguments) {
        this.code = code;
        this.message = message;
        this.arguments = arguments;
    }
}
