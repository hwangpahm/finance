package com.mycompany.myapp.error.exceptions;

import org.springframework.core.NestedRuntimeException;

public abstract class UncheckedException extends NestedRuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7544223281433256393L;

    private final ErrorBase error;

    protected UncheckedException(ErrorBase error) {
        super(error.getMessage());
        this.error = error;
    }

    protected UncheckedException(String code, String message) {
        super(message);
        this.error = new ErrorBase(code, message);
    }

    protected UncheckedException(String code, String message, Object[] args) {
        super(message);
        this.error = new ErrorBase(code, message, args);
    }

    protected UncheckedException(String code, String message, Throwable cause) {
        super(message, cause);
        this.error = new ErrorBase(code, message);
    }

    protected UncheckedException(String code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.error = new ErrorBase(code, message, args);
    }

    /**
     * @return the error
     */
    public ErrorBase getError() {
        return error;
    }

}
