package com.mycompany.myapp.error.exceptions;

public class ServiceFailedException extends UncheckedException {

    private static final long serialVersionUID = 1L;

    /**
     * @param code
     * @param message
     * @param args
     */
    public ServiceFailedException(String code, String message, Object[] args) {
        super(code, message, args);
    }

    /**
     * @param code
     * @param message
     * @param cause
     * @param args
     */
    public ServiceFailedException(String code, String message, Throwable cause, Object[] args) {
        super(code, message, cause, args);
    }

    /**
     * @param code
     * @param message
     * @param cause
     */
    public ServiceFailedException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    /**
     * @param code
     * @param message
     */
    public ServiceFailedException(String code, String message) {
        super(code, message);
    }

    public ServiceFailedException(ErrorBase error) {
        super(error);
    }
}
