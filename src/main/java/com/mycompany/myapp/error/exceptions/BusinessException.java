package com.mycompany.myapp.error.exceptions;

import org.springframework.core.NestedCheckedException;

import java.util.ArrayList;
import java.util.List;

public abstract class BusinessException extends NestedCheckedException {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2223663039483348747L;

    /**
     * errors
     */
    private transient final List<ErrorBase> errors = new ArrayList<>();

    /**
     * @param code
     * @param message
     */
    protected BusinessException(String code, String message) {
        super(message);
        addError(new ErrorBase(code, message));
    }

    /**
     * @param code
     * @param message
     * @param args
     */
    protected BusinessException(String code, String message, Object[] args) {
        super(message);
        addError(new ErrorBase(code, message, args));
    }

    /**
     * @param code
     * @param message
     * @param cause
     */
    protected BusinessException(String code, String message, Throwable cause) {
        super(message, cause);
        addError(new ErrorBase(code, message));
    }

    /**
     * @param code
     * @param message
     * @param cause
     * @param args
     */
    protected BusinessException(String code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        addError(new ErrorBase(code, message, args));
    }

    /**
     * @param error
     */
    protected BusinessException(ErrorBase error) {
        super(error.getMessage());
        addError(error);
    }

    /**
     * @param errors
     */
    protected BusinessException(List<ErrorBase> errors) {
        super(errors.get(0).getMessage());
        addAllErrors(errors);
    }

    /**
     * @return the error
     */
    public ErrorBase getError() {
        return errors.get(0);
    }

    /**
     * @return the list of errors
     */
    public List<ErrorBase> getAllErrors() {
        return errors;
    }

    /**
     * @param code
     * @param message
     */
    public void addError(String code, String message) {
        this.errors.add(new ErrorBase(code, message));
    }

    /**
     * @param code
     * @param message
     * @param arguments
     */
    public void addError(String code, String message, Object[] arguments) {
        this.errors.add(new ErrorBase(code, message, arguments));
    }

    /**
     * @param error
     */
    public void addError(ErrorBase error) {
        this.errors.add(error);
    }

    /**
     * @param errors
     */
    public void addAllErrors(List<ErrorBase> errors) {
        this.errors.addAll(errors);
    }

}
