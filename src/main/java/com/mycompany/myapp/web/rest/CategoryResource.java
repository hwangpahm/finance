package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.component.CategoryComponent;
import com.mycompany.myapp.component.dto.CategoryDTO;
import com.mycompany.myapp.component.vo.CategoryVO;

import com.mycompany.myapp.error.exceptions.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
@Validated
public class CategoryResource {

    private final CategoryComponent categoryComponent;

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    public CategoryResource(CategoryComponent categoryComponent) {
        this.categoryComponent = categoryComponent;
    }

    @PostMapping("/categories")
    public ResponseEntity<List<CategoryDTO>> createCategories(@Valid @RequestBody List<CategoryVO> categoryVOS) throws ValidationException {
        log.debug("================Create Categories==========");
        return new ResponseEntity<>(categoryComponent.createCategories(categoryVOS), HttpStatus.OK);
    }

    @GetMapping("/categories")
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        log.debug("================Get All Categories==========");
        return new ResponseEntity<>(categoryComponent.getAllCategories(), HttpStatus.OK);
    }

}
