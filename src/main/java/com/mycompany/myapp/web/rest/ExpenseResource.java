package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.common.constants.RegexPattern;
import com.mycompany.myapp.component.ExpenseComponent;
import com.mycompany.myapp.component.dto.ExpenseDTO;
import com.mycompany.myapp.component.vo.ExpenseVO;
import com.mycompany.myapp.error.exceptions.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
@Validated
public class ExpenseResource {

    private final ExpenseComponent expenseComponent;

    public ExpenseResource(ExpenseComponent expenseComponent) {
        this.expenseComponent = expenseComponent;
    }

    @PostMapping("/expenses")
    public ResponseEntity<ExpenseDTO> createCategories(@Valid @RequestBody ExpenseVO expenseVO) {
        return new ResponseEntity<>(expenseComponent.createExpense(expenseVO), HttpStatus.OK);
    }

    @GetMapping("/expenses/{targetDate}")
    public ResponseEntity<List<ExpenseDTO>> getAllExpensesByDate(@PathVariable("targetDate")
                                                           @Pattern(regexp = RegexPattern.DATE_FORMAT_YYYMMDD, message = "{validate.core.target-date.invalid}")
                                                           String targetDate) {
        return new ResponseEntity<>(expenseComponent.getAllExpensesByDate(targetDate), HttpStatus.OK);
    }

    @GetMapping("/expenses/{targetDate}/{category}")
    public ResponseEntity<List<ExpenseDTO>> getAllExpensesByDate(@PathVariable("targetDate")
                                                                 @Pattern(regexp = RegexPattern.DATE_FORMAT_YYYMMDD, message = "{validate.core.target-date.invalid}")
                                                                 String targetDate,
                                                                 @PathVariable("category") String category) throws ValidationException {
        return new ResponseEntity<>(expenseComponent.getAllExpensesByDateAndCategory(targetDate, category), HttpStatus.OK);
    }

    @GetMapping("/expenses")
    public ResponseEntity<List<ExpenseDTO>> getAllExpensesByCategory(@RequestParam ("category") String category) throws ValidationException {
        return new ResponseEntity<>(expenseComponent.getAllExpensesByCategory(category), HttpStatus.OK);
    }

    @DeleteMapping("/expenses/{id}")
    public ResponseEntity<Void> deleteExpenseById(@PathVariable("id") Integer id) throws ValidationException {
        expenseComponent.deleteExpenseById(id);
        return null;
    }

    @GetMapping("/expenses/month/{targetMonth}")
    public ResponseEntity<List<ExpenseDTO>> getAllExpensesByMonth(@PathVariable("targetMonth")
                                                                 @Pattern(regexp = RegexPattern.DATE_FORMAT_YYYMM, message = "{validate.core.target-date.invalid}")
                                                                 String targetMonth) throws ValidationException {
        return new ResponseEntity<>(expenseComponent.getAllExpensesInMonth(targetMonth), HttpStatus.OK);
    }
}
