package com.mycompany.myapp.domain.finance;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "expense")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpenseEntity implements Serializable {

    private static final long serialVersionUID = 103199491786493055L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "category_id", nullable = false)
    private Integer categoryId;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "note", nullable = false)
    private String note;

    @Column(name = "expense", nullable = false)
    private Integer expense;
}
