package com.mycompany.myapp.domain.finance;

import com.mycompany.myapp.common.convert.CategoryExpenseConverter;
import com.mycompany.myapp.common.convert.CategoryIncomeConverter;
import com.mycompany.myapp.common.convert.CategoryTypeConverter;
import com.mycompany.myapp.common.enums.CategoryExpense;
import com.mycompany.myapp.common.enums.CategoryIncome;
import com.mycompany.myapp.common.enums.CategoryType;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "category")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
public class CategoryEntity implements Serializable {

    private static final long serialVersionUID = -4740202546776118653L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "category", nullable = false)
    private String category;

    @Column(name = "type", nullable = false)
    @Convert(converter = CategoryTypeConverter.class)
    private CategoryType type;
}
