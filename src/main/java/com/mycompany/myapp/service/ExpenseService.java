package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.finance.ExpenseEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ExpenseService {

    ExpenseEntity createNewExpense(ExpenseEntity expenseEntity);

    void deleteExpense(ExpenseEntity expenseEntity);

    List<ExpenseEntity> getAllExpensesByDate(Date targetDate);

    List<ExpenseEntity> getAllExpensesByDateAndCategory(Date targetDate, Integer categoryId);

    List<ExpenseEntity> getAllExpensesByCategory(Integer categoryId);

    Optional<ExpenseEntity> findById(Integer id);

    List<ExpenseEntity> findExpensesDateBetween(Date startDate, Date endDate);
}
