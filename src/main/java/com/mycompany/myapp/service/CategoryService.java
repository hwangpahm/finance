package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.finance.CategoryEntity;

import java.util.List;

public interface CategoryService {

    List<CategoryEntity> createCategories(List<CategoryEntity> categoryEntities);

    List<CategoryEntity> findAllCategories();

    List<CategoryEntity> findByCategoryIn(List<String> categories);

    List<CategoryEntity> findByCategoryIdIn(List<Integer> ids);

    CategoryEntity createCategory(CategoryEntity category);
}
