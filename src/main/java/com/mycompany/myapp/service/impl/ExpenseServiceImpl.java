package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.finance.ExpenseEntity;
import com.mycompany.myapp.repository.ExpenseRepository;
import com.mycompany.myapp.service.ExpenseService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;

    public ExpenseServiceImpl(ExpenseRepository expenseRepository) {
        this.expenseRepository = expenseRepository;
    }


    @Override
    public ExpenseEntity createNewExpense(ExpenseEntity expenseEntity) {
        return expenseRepository.save(expenseEntity);
    }

    @Override
    public void deleteExpense(ExpenseEntity expenseEntity) {
        expenseRepository.delete(expenseEntity);
    }

    @Override
    public List<ExpenseEntity> getAllExpensesByDate(Date targetDate) {
        return expenseRepository.findByDate(targetDate);
    }

    @Override
    public List<ExpenseEntity> getAllExpensesByDateAndCategory(Date targetDate, Integer categoryId) {
        return expenseRepository.findByDateAndCategoryId(targetDate, categoryId);
    }

    @Override
    public List<ExpenseEntity> getAllExpensesByCategory(Integer categoryId) {
        return expenseRepository.findByCategoryId(categoryId);
    }

    @Override
    public Optional<ExpenseEntity> findById(Integer id) {
        return expenseRepository.findById(id);
    }

    @Override
    public List<ExpenseEntity> findExpensesDateBetween(Date startDate, Date endDate) {
        return expenseRepository.findByDateBetween(startDate, endDate);
    }
}
