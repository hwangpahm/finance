package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.finance.CategoryEntity;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<CategoryEntity> createCategories(List<CategoryEntity> categoryEntities) {
        return categoryRepository.saveAll(categoryEntities);
    }

    @Override
    public List<CategoryEntity> findAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public List<CategoryEntity> findByCategoryIn(List<String> categories) {
        return categoryRepository.findByCategoryIn(categories);
    }

    @Override
    public List<CategoryEntity> findByCategoryIdIn(List<Integer> ids) {
        return categoryRepository.findAllById(ids);
    }

    @Override
    public CategoryEntity createCategory(CategoryEntity category) {
        return categoryRepository.save(category);
    }
}
