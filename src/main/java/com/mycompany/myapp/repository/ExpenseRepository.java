package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.finance.ExpenseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ExpenseRepository extends JpaRepository<ExpenseEntity, Integer> {

    List<ExpenseEntity> findByDate(Date date);

    List<ExpenseEntity> findByDateAndCategoryId(Date date, Integer categoryId);

    List<ExpenseEntity> findByCategoryId(Integer categoryId);

    List<ExpenseEntity> findByDateBetween(Date startDate, Date endDate);

}
