package com.mycompany.myapp.component;

import com.mycompany.myapp.component.dto.ExpenseDTO;
import com.mycompany.myapp.component.vo.ExpenseVO;
import com.mycompany.myapp.error.exceptions.ValidationException;
import io.swagger.models.auth.In;

import java.util.List;

public interface ExpenseComponent {

    ExpenseDTO createExpense(ExpenseVO expenseVO);

    List<ExpenseDTO> getAllExpensesByDate(String targetDate);

    List<ExpenseDTO> getAllExpensesByDateAndCategory(String targetDate, String category) throws ValidationException;

    List<ExpenseDTO> getAllExpensesByCategory(String category) throws ValidationException;

    List<ExpenseDTO> getAllExpensesInMonth (String targetMonth) throws ValidationException;

    void deleteExpenseById(Integer id) throws ValidationException;
}
