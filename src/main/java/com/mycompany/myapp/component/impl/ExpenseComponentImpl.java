package com.mycompany.myapp.component.impl;

import com.mycompany.myapp.common.DateTimeUtils;
import com.mycompany.myapp.common.configs.ErrorMessage;
import com.mycompany.myapp.common.constants.DateFormatConstant;
import com.mycompany.myapp.common.constants.ErrorCode;
import com.mycompany.myapp.common.enums.CategoryType;
import com.mycompany.myapp.component.ExpenseComponent;
import com.mycompany.myapp.component.dto.ExpenseDTO;
import com.mycompany.myapp.component.mapper.ExpenseMapper;
import com.mycompany.myapp.component.vo.ExpenseVO;
import com.mycompany.myapp.domain.finance.CategoryEntity;
import com.mycompany.myapp.domain.finance.ExpenseEntity;
import com.mycompany.myapp.error.exceptions.ValidationException;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.ExpenseService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExpenseComponentImpl implements ExpenseComponent {
    private final ExpenseService expenseService;

    private final ExpenseMapper expenseMapper;

    private final CategoryService categoryService;

    private final ErrorMessage errorMessage;

    public ExpenseComponentImpl(ExpenseService expenseService, ExpenseMapper expenseMapper, CategoryService categoryService, ErrorMessage errorMessage) {
        this.expenseService = expenseService;
        this.expenseMapper = expenseMapper;
        this.categoryService = categoryService;
        this.errorMessage = errorMessage;
    }

    @Override
    @Transactional
    public ExpenseDTO createExpense(ExpenseVO expenseVO) {
        var categoryOpt = categoryService.findByCategoryIn(List.of(expenseVO.getCategory())).stream().findFirst();
        var categoryEntity = new CategoryEntity();
        if (categoryOpt.isPresent()) {
            categoryEntity = categoryOpt.get();
        } else {
            categoryEntity.setCategory(expenseVO.getCategory());
            categoryEntity.setType(CategoryType.EXPENSE);
            categoryService.createCategory(categoryEntity);
        }
        var expenseEntity = new ExpenseEntity();
        expenseMapper.convertToExpenseEntity(expenseEntity, expenseVO, categoryEntity);
        expenseService.createNewExpense(expenseEntity);

        return expenseMapper.convertToExpenseDTO(expenseEntity, categoryEntity);
    }

    @Override
    public List<ExpenseDTO> getAllExpensesByDate(String targetDate) {
        var date = DateTimeUtils.parseDate(targetDate, DateFormatConstant.DATE_FORMAT_NOT_SPACE);
        var expenseEntities = expenseService.getAllExpensesByDate(date);
        var categoryIds = expenseEntities.stream().map(ExpenseEntity::getCategoryId).collect(Collectors.toList());
        var categoryEntities = categoryService.findByCategoryIdIn(categoryIds);
        List<ExpenseDTO> expenseDTOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(expenseEntities)) {
            expenseEntities.forEach(e -> {
                var categoryOpt = categoryEntities.stream().filter(category -> e.getCategoryId().equals(category.getId())).findFirst();
                categoryOpt.ifPresent(category -> expenseDTOS.add(expenseMapper.convertToExpenseDTO(e, category)));
            });
        }
        return expenseDTOS;
    }

    @Override
    public List<ExpenseDTO> getAllExpensesByDateAndCategory(String targetDate, String category) throws ValidationException {
        var date = DateTimeUtils.parseDate(targetDate, DateFormatConstant.DATE_FORMAT_NOT_SPACE);
        var categoryOpt = categoryService.findByCategoryIn(List.of(category.toUpperCase())).stream().findFirst();
        if (categoryOpt.isEmpty()) {
            throw new ValidationException(errorMessage.getError(ErrorCode.RECORD_NOT_FOUND));
        }
        var expenseEntities = expenseService.getAllExpensesByDateAndCategory(date, categoryOpt.get().getId());
        List<ExpenseDTO> expenseDTOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(expenseEntities)) {
            expenseEntities.forEach(e -> expenseDTOS.add(expenseMapper.convertToExpenseDTO(e, categoryOpt.get())));
        }
        return expenseDTOS;
    }

    @Override
    public List<ExpenseDTO> getAllExpensesByCategory(String category) throws ValidationException {
        var categoryOpt = categoryService.findByCategoryIn(List.of(category.toUpperCase())).stream().findFirst();
        if (categoryOpt.isEmpty()) {
            throw new ValidationException(errorMessage.getError(ErrorCode.RECORD_NOT_FOUND));
        }
        var expenseEntities = expenseService.getAllExpensesByCategory(categoryOpt.get().getId());
        List<ExpenseDTO> expenseDTOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(expenseEntities)) {
            expenseEntities.forEach(e -> expenseDTOS.add(expenseMapper.convertToExpenseDTO(e, categoryOpt.get())));
        }
        return expenseDTOS;
    }

    @Override
    public List<ExpenseDTO> getAllExpensesInMonth(String targetMonth) throws ValidationException {
        var dateDuration = DateTimeUtils.getStartDateAndEndDate(targetMonth);
        var startDate = dateDuration.getStartDate();
        var endDate = dateDuration.getEndDate();

        var expenseEntities = expenseService.findExpensesDateBetween(startDate, endDate);
        var categoryIds = expenseEntities.stream().map(ExpenseEntity::getCategoryId).collect(Collectors.toList());
        var categoryEntities = categoryService.findByCategoryIdIn(categoryIds);
        List<ExpenseDTO> expenseDTOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(expenseEntities)) {
            expenseEntities.forEach(e -> {
                var categoryOpt = categoryEntities.stream().filter(category -> e.getCategoryId().equals(category.getId())).findFirst();
                categoryOpt.ifPresent(category -> expenseDTOS.add(expenseMapper.convertToExpenseDTO(e, category)));
            });
        }
        return expenseDTOS;
    }

    @Override
    public void deleteExpenseById(Integer id) throws ValidationException {
        var expenseOpt = expenseService.findById(id);
        if (expenseOpt.isEmpty()) {
            throw new ValidationException(errorMessage.getError(ErrorCode.RECORD_NOT_FOUND));
        }
        expenseService.deleteExpense(expenseOpt.get());
    }
}
