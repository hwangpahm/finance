package com.mycompany.myapp.component.impl;

import com.mycompany.myapp.common.configs.ErrorMessage;
import com.mycompany.myapp.common.constants.ErrorCode;
import com.mycompany.myapp.component.CategoryComponent;
import com.mycompany.myapp.component.dto.CategoryDTO;
import com.mycompany.myapp.component.mapper.CategoryMapper;
import com.mycompany.myapp.component.vo.CategoryVO;
import com.mycompany.myapp.domain.finance.CategoryEntity;
import com.mycompany.myapp.error.exceptions.ValidationException;
import com.mycompany.myapp.service.CategoryService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryComponentImpl implements CategoryComponent {

    private final CategoryService categoryService;

    private final CategoryMapper categoryMapper;

    private final ErrorMessage errorMessage;

    public CategoryComponentImpl(CategoryService categoryService, CategoryMapper categoryMapper, ErrorMessage errorMessage) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.errorMessage = errorMessage;
    }

    @Override
    public List<CategoryDTO> createCategories(List<CategoryVO> categoryVOS) throws ValidationException {
        List<CategoryDTO> categoryDTOS = new ArrayList<>();
        List<CategoryEntity> categoryEntities = new ArrayList<>();
        if (!CollectionUtils.isEmpty(categoryVOS)) {
            var categoryNames = categoryVOS.stream().map(CategoryVO::getCategory).collect(Collectors.toList());
            var categoryEntityList = categoryService.findByCategoryIn(categoryNames);
            if (!CollectionUtils.isEmpty(categoryEntityList)) {
                throw new ValidationException(errorMessage.getError(ErrorCode.CategoryErrorCode.CATEGORY_EXISTS, CategoryEntity.Fields.category));
            }
            categoryVOS.forEach(vo -> {
                var category = new CategoryEntity();
                categoryEntities.add(categoryMapper.convertToCategoryEntity(category, vo));
            });
            categoryService.createCategories(categoryEntities);
            categoryEntities.forEach(c -> categoryDTOS.add(categoryMapper.convertToCategoryDTO(c)));
        }

        return categoryDTOS;
    }

    @Override
    public List<CategoryDTO> getAllCategories() {
        var categoryEntities = categoryService.findAllCategories();
        return categoryEntities.stream().map(categoryMapper::convertToCategoryDTO).collect(Collectors.toList());
    }
}
