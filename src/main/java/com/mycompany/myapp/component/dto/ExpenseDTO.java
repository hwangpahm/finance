package com.mycompany.myapp.component.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExpenseDTO implements Serializable {

    private static final long serialVersionUID = -6200677638526783925L;
    private Integer id;
    private String date;
    private String categoryName;
    private String note;
    private Integer expense;
}
