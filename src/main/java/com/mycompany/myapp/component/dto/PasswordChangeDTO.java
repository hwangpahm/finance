package com.mycompany.myapp.component.dto;

import lombok.*;

/**
 * A DTO representing a password change required data - current and new password.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PasswordChangeDTO {

    private String currentPassword;
    private String newPassword;
}
