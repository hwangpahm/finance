package com.mycompany.myapp.component.dto;

import com.mycompany.myapp.domain.User;
import lombok.*;
import lombok.experimental.FieldNameConstants;

/**
 * A DTO representing a user, with only the public attributes.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
public class UserDTO {

    private Long id;

    private String login;

    public UserDTO(User user) {
        this.id = user.getId();
        // Customize it here if you need, or not, firstName/lastName/etc
        this.login = user.getLogin();
    }
}
