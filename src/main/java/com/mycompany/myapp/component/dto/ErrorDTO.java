package com.mycompany.myapp.component.dto;

import com.mycompany.myapp.error.exceptions.ErrorBase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDTO implements Serializable {

    private static final long serialVersionUID = 7084482713497826747L;

    private List<ErrorBase> errors;

    public ErrorDTO(ErrorBase error) {
        this.errors = List.of(error);
    }
}
