package com.mycompany.myapp.component.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryDTO implements Serializable {

    private static final long serialVersionUID = -3478255318465569679L;
    private Integer id;
    private String category;
    private String type;
}
