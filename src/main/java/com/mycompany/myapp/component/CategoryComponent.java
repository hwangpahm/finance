package com.mycompany.myapp.component;

import com.mycompany.myapp.component.dto.CategoryDTO;
import com.mycompany.myapp.component.vo.CategoryVO;
import com.mycompany.myapp.error.exceptions.ValidationException;

import java.util.List;

public interface CategoryComponent {

    List<CategoryDTO> createCategories (List<CategoryVO> categoryVOS) throws ValidationException;

    List<CategoryDTO> getAllCategories();
}
