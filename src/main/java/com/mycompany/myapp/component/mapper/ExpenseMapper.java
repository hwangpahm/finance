package com.mycompany.myapp.component.mapper;

import com.mycompany.myapp.common.DateTimeUtils;
import com.mycompany.myapp.common.constants.DateFormatConstant;
import com.mycompany.myapp.component.dto.ExpenseDTO;
import com.mycompany.myapp.component.vo.ExpenseVO;
import com.mycompany.myapp.domain.finance.CategoryEntity;
import com.mycompany.myapp.domain.finance.ExpenseEntity;
import org.springframework.stereotype.Component;

@Component
public class ExpenseMapper {

    public ExpenseEntity convertToExpenseEntity(final ExpenseEntity expenseEntity, ExpenseVO expenseVO, CategoryEntity category) {
        expenseEntity.setCategoryId(category.getId());
        expenseEntity.setDate(DateTimeUtils.parseDate(expenseVO.getDate(), DateFormatConstant.DATE_FORMAT));
        expenseEntity.setExpense(expenseVO.getExpense());
        expenseEntity.setNote(expenseVO.getNote());
        return expenseEntity;
    }

    public ExpenseDTO convertToExpenseDTO (ExpenseEntity expenseEntity, CategoryEntity category) {
        var expenseDTO = ExpenseDTO.builder();
        return expenseDTO.id(expenseEntity.getId())
            .date(DateTimeUtils.formatTime(expenseEntity.getDate(), DateFormatConstant.DATE_FORMAT))
            .categoryName(category.getCategory())
            .note(expenseEntity.getNote())
            .expense(expenseEntity.getExpense())
            .build();
    }
}
