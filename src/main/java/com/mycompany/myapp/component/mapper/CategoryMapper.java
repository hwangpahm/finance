package com.mycompany.myapp.component.mapper;

import com.mycompany.myapp.common.enums.CategoryIncome;
import com.mycompany.myapp.common.enums.CategoryType;
import com.mycompany.myapp.component.dto.CategoryDTO;
import com.mycompany.myapp.component.vo.CategoryVO;
import com.mycompany.myapp.domain.finance.CategoryEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CategoryMapper {

    public CategoryEntity convertToCategoryEntity (final CategoryEntity category, CategoryVO categoryVO) {
        category.setCategory(categoryVO.getCategory());
        category.setType(CategoryType.fromCode(categoryVO.getType()));
        return category;
    }

    public CategoryDTO convertToCategoryDTO (CategoryEntity category) {
        var categoryDTO = CategoryDTO.builder();
        categoryDTO.id(category.getId())
            .category(category.getCategory())
            .type(Optional.of(category.getType()).map(Enum::toString).orElse(null));
        return categoryDTO.build();
    }
}
