package com.mycompany.myapp.component.vo;

import com.mycompany.myapp.common.constants.RegexPattern;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class ExpenseVO implements Serializable {

    private static final long serialVersionUID = 5318407199023285123L;

    @NotNull(message = "{validate.core.common.field.required}")
    @Size(max = 25, message = "{validate.core.common.number.max}")
    private String category;

    @NotNull(message = "{validate.core.common.field.required}")
    @Pattern(regexp = RegexPattern.DATE_FORMAT, message = "{validate.core.target-date.invalid}")
    private String date;

    @NotNull(message = "{validate.core.common.field.required}")
    private Integer expense;

    private String note;
}
