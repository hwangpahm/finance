package com.mycompany.myapp.component.vo;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.springframework.context.MessageSource;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class CategoryVO implements Serializable {

    private static final long serialVersionUID = -7764066986439045230L;

    @NotNull(message = "{validate.core.common.field.required}")
    @Size(max = 25, message = "{validate.core.common.number.max}")
    private String category;

    @NotNull(message = "{validate.core.common.field.required}")
    private Integer type;
}
