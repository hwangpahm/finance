CREATE TABLE if not exists category
(
    id       serial              NOT NULL,
    category VARCHAR(255)        NOT NULL,
    type     int4                NOT NULL,
    CONSTRAINT pk_category PRIMARY KEY (id)
);
