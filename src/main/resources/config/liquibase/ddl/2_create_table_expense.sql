CREATE TABLE if not exists expense
(
    id          serial NOT NULL,
    category_id int4                                  NOT NULL,
    date        timestamp              NOT NULL,
    note        VARCHAR(255)                             NOT NULL,
    expense     int4                                  NOT NULL,
    CONSTRAINT pk_expense PRIMARY KEY (id)
);
