CREATE TABLE if not exists income
(
    id          serial            NOT NULL,
    category_id int4              NOT NULL,
    date        TIMESTAMP         NOT NULL,
    note        VARCHAR(255)      NOT NULL,
    income      int4              NOT NULL,
    CONSTRAINT pk_income PRIMARY KEY (id)
);
